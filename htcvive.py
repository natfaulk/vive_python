import pygame
import sys
import triad_openvr
import matplotlib.path as mpltPath
import time
import os

SCREEN_WIDTH_PX = 800
SCREEN_HEIGHT_PX = 600
SCREEN_SCALE = SCREEN_HEIGHT_PX / 5
SCREEN_REF_PX = (SCREEN_WIDTH_PX / 2, 50)

TRACKER_KEY = "tracker_1"
MAX_TRACKER_TRAIL = 50

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
PURPLE = (255, 0, 255)
GREEN = (0, 255, 0)

latestpos = []

def M_to_Screen(x, y):
  xt = SCREEN_REF_PX[0] + x * SCREEN_SCALE
  yt = SCREEN_REF_PX[1] - y * SCREEN_SCALE
  return (xt, yt)

def inside_poly(_p, _anchors):
  # l = [list(i) for i in _anchors]
  l = _anchors
  path = mpltPath.Path(l)
  return path.contains_point(_p)

def draw(_screen, _anchors, _tracker):
  _screen.fill(BLACK)
  for i in _anchors:
    pygame.draw.ellipse(_screen, BLUE, (i[0] - 5, i[1] - 5, 10, 10))

  if len(_anchors) >= 4:
    pygame.draw.polygon(_screen, BLUE, _anchors, 2)

  pos = pygame.mouse.get_pos()
  col = PURPLE
  
  if len(_anchors) >= 4 and inside_poly(pos, _anchors):
    col = GREEN
  pygame.draw.ellipse(_screen, col, (pos[0] - 5, pos[1] - 5, 10, 10))
  for t in _tracker:
    pygame.draw.ellipse(_screen, WHITE, (t[0] - 2.5, t[1] - 2.5, 5, 5))
  pygame.display.flip()


def begin(_callback):
  global latestpos
  anchors = []
  tracker = []

  mouseClick = False
  setupDone = False

  v = triad_openvr.triad_openvr()
  v.print_discovered_objects()

  data = [[],[],[]]

  pygame.init()
  screen = pygame.display.set_mode((800, 600))
  pygame.display.set_caption("Pygame test")
  screen.fill(BLACK)
  pygame.display.flip()

  clock = pygame.time.Clock()
  while True:
    # needed else other threads lock up
    time.sleep(0.01)
    clock.tick(60)
  
    for event in pygame.event.get():
      if event.type == pygame.QUIT:
        sys.exit()
      if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_c:
          tracker = []
      if event.type == pygame.MOUSEBUTTONUP:
        mouseClick = True
        
    if TRACKER_KEY in v.devices:
      data = []
      latestpos = v.devices[TRACKER_KEY].get_pose_euler()
      data.append(v.devices[TRACKER_KEY].get_pose_euler()[0])
      data.append(v.devices[TRACKER_KEY].get_pose_euler()[1])
      data.append(v.devices[TRACKER_KEY].get_pose_euler()[2])
      
      dt = M_to_Screen(data[0], data[2])
      tracker.append(dt)
      if len(tracker) > MAX_TRACKER_TRAIL:
        tracker = tracker[-MAX_TRACKER_TRAIL:]
      # pygame.draw.ellipse(screen, WHITE, (dt[0], dt[1], 5, 5))
    
    _callback((len(anchors) >= 4) and mouseClick)

    if mouseClick:
      mouseClick = False
      if TRACKER_KEY in v.devices:
        pos = dt
      else:
        pos = pygame.mouse.get_pos()
      print(pos)
      if len(anchors) < 4:
        anchors.append(pos)
        
        if len(anchors) == 4:
          f = open("outputs{}anchors.txt".format(os.sep), "w")
          for i in anchors:
            f.write(str(i))
          f.close()

    draw(screen, anchors, tracker)
    if len(anchors) >= 4:
      setupDone = True

  print("Setup done")

  # while True:
  #   clock.tick(60)
  #   # sock_handler()
  
  #   for event in pygame.event.get():
  #     if event.type == pygame.QUIT:
  #       sys.exit()
  #     if event.type == pygame.KEYDOWN:
  #       if event.key == pygame.K_c:
  #         tracker = []
  #     if event.type == pygame.MOUSEBUTTONUP:
  #       mouseClick = True
  #   draw(screen, anchors, tracker)



# import socket
# import sys
# from urllib.parse import urlencode
# from urllib.request import Request, urlopen

# def get_ip():
#   s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#   try:
#     # doesn't even have to be reachable
#     s.connect(('10.255.255.255', 1))
#     IP = s.getsockname()[0]
#   except:
#     IP = '127.0.0.1'
#   finally:
#     s.close()
#   return IP

# print('This ip is: ', get_ip())

# # Create a TCP/IP socket
# sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# # Bind the socket to the port
# server_address = (get_ip(), 10000)
# print('starting up on {} port {}'.format(*server_address))
# sock.bind(server_address)
# sock.listen(1)

# def sock_handler():
#   # Wait for a connection
#   print('waiting for a connection')
#   connection, client_address = sock.accept()
#   try:
#     print('connection from', client_address)

#     # Receive the data in small chunks and retransmit it
#     while True:
#       data = connection.recv(16)
#       print('received {!r}'.format(data))
#       if data:
#         print('sending data back to the client')
        
#         if TRACKER_KEY in v.devices:
#           pos = v.devices[TRACKER_KEY].get_pose_euler()
#         else:
#           pos = pygame.mouse.get_pos()
#         connection.sendall(str.encode(str(pos)))
#       else:
#         print('no data from', client_address)
#         break

#   finally:
#     # Clean up the connection
#     connection.close()

# connection, client_address = sock.accept()
# print('Connected with ' + client_address[0] + ':' + str(client_address[1]))
# while True:
#   clock.tick(60)
#   # sock_handler()
 
#   for event in pygame.event.get():
#     if event.type == pygame.QUIT:
#       sys.exit()
#     if event.type == pygame.KEYDOWN:
#       if event.key == pygame.K_c:
#         tracker = []
#     if event.type == pygame.MOUSEBUTTONUP:
#       mouseClick = True
#   draw(screen, anchors, tracker)


