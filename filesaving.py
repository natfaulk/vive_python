import os.path

currentIndex = 0

def nextFilename(_path, _prefix, _suffix):
  global currentIndex
  while os.path.isfile("{}{}{}{}.{}".format(_path, os.sep, _prefix, currentIndex, _suffix)):
    currentIndex += 1
  return "{}{}{}{}.{}".format(_path, os.sep, _prefix, currentIndex, _suffix)

def mkdirp(_path):
  if not os.path.isdir(_path):
    os.makedirs(_path)