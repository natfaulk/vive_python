import time
import signal
import random
from baseThread import BaseThread
import wsRec
import htcvive
import filesaving
 
counter = 0
threadID = 0
run = True

class PrintingThread(BaseThread):
  def run(self):
    while run:
      time.sleep(1)
      self.print("Counter: {}".format(counter))
    self.print("Thread stopped")

# class IncrementThreads(BaseThread):
#   def run(self):
#     # global counter
#     while run:
#       time.sleep(0.05)
#       for i in range(random.randrange(0, 5)):
#         moduletest.incVal()

#     self.print("Thread stopped")

# t = PrintingThread(threadID)
# threadID += 1
# t.start()

t = wsRec.WsThread(threadID)
threadID += 1
t.daemon = True
t.start()

# htc = HtcViveThread(threadID)
# threadID += 1
# htc.start()

filesaving.mkdirp("outputs")
f = open(filesaving.nextFilename("outputs", "test", "txt"), "w")

def sigint_handler(signum, frame):
  # htc.ctrl_c = True
  print("Exiting program")
  f.close()
 
signal.signal(signal.SIGINT, sigint_handler)

saving = False
savecount = 0
def loopCallback(savedata):
  global f
  global savecount
  global saving
  if not saving and savedata:
    saving = True
    savecount = 10
    print("Began save")
  
  if saving and savecount > 0:
    if wsRec.dataAvail():
      f.write(str(wsRec.getLatestData()))
      f.write(str(htcvive.latestpos))
      savecount -= 1
      print("Save {}".format(savecount))
  elif saving:
    saving = False
    f.close()
    f = open(filesaving.nextFilename("outputs", "test", "txt"), "w")
    print("Finished save")


htcvive.begin(loopCallback)

# for i in range(3):
#   t = IncrementThreads(threadID)
#   threadID += 1
#   t.start()

# while run:
#   time.sleep(0.1)
print("Main thread stopped")
