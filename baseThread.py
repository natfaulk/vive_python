import threading

class BaseThread(threading.Thread):
  def __init__(self, id):
    threading.Thread.__init__(self)
    self.id = id
    self.print("Thread started")
  
  def print(self, s):
    print("[{}] {}".format(self.id, s))