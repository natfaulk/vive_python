from gevent import monkey; monkey.patch_all()
from ws4py.websocket import WebSocket
from ws4py.server.geventserver import WSGIServer
from ws4py.server.wsgiutils import WebSocketWSGIApplication

from urllib.parse import urlencode
from urllib.request import Request, urlopen
import socket

from baseThread import BaseThread

# class FakePrint():
#   def print(self, s):
#     return

latestData = []
newData = False

def dataAvail():
  return newData

def getLatestData():
  global newData
  newData = False
  return latestData

class EchoWebSocket(WebSocket):
  # def __init__(self, *args, **kwargs):
  #   WebSocket.__init__(*args, **kwargs)
  #   self.print_o = FakePrint()

  # def setPrintO(self, p):
  #   self.print_o = p

  def received_message(self, message):
    global latestData
    global newData
    # self.print(message.data)
    # self.print_o("Ws received data")
    print("Ws received data")
    latestData = message.data
    newData = True

  def opened(self):
    # self.print_o("websocket opened")
    print("websocket opened")
  
  def closed(self, code, reason=None):
    # self.print_o("websocket closed")
    print("websocket closed")

def get_ip():
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  try:
    # doesn't even have to be reachable
    s.connect(('10.255.255.255', 1))
    IP = s.getsockname()[0]
  except:
    IP = '127.0.0.1'
  finally:
    s.close()
  return IP

class WsThread(BaseThread):
  def run(self):
    self.print('This ip is: {}'.format(get_ip()))
    url = 'http://vlp.natfaulk.com/set_pc_ip'
    post_fields = {'ip': get_ip()}

    # need user agent else gets blocked (by cloudflare?)
    request = Request(url, urlencode(post_fields).encode(), headers={'User-Agent': 'Mozilla/5.0'})
    res = urlopen(request).read().decode()
    self.print("Response was: {}".format(res))

    server = WSGIServer((get_ip(), 8000), WebSocketWSGIApplication(handler_cls=EchoWebSocket))
    server.serve_forever()
    self.print("Ws thread stopped")
    